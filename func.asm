section .text
global Decode39
extern table

Decode39:
    push    ebp
	push	edi
	push	esi
	push    ebx
    mov     ebp, esp

	xor		ecx, ecx	; current x pixel
skipping_white_space:
	inc 	ecx			;increment until black pixel is found
	call 	is_pixel_black
	;if pixel is not black, go back
	cmp		eax, 0
	jz		skipping_white_space
	
	push	ecx			;number of first black pixel

counting_narrow:
	inc		ecx			;increment until white pixel is found
	;checking if pixel is white
	call	is_pixel_black
	cmp 	eax, 0
	
	jnz		counting_narrow
	
	mov		eax, ecx
	pop		ebx
	sub		eax, ebx		
	push	eax		; push length of narrow space to [ebp - 4]
	
	push	ecx

counting_wide:
	inc 	ecx		;increment until black pixel is found
	call	is_pixel_black
	;if pixel is not white, go back
	cmp		eax, 0
	jz		counting_wide

	mov		eax, ecx
	pop		ebx
	sub		eax, ebx
	push	eax		; push length of wide space to [ebp - 8]

	; skip rest of * character
	; skip two wide spaces
	lea		ecx, [ecx + eax*2] ; eax is still length of wide space
	;skip six narrow spaces
	mov		eax, [ebp-4]
	lea 	eax, [eax + eax*2]
	lea		ecx, [ecx + eax*2]
	inc     ecx

	xor		esi, esi	; length of result 

    push    0           ; number of current bit [ebp - 12]
    push    0           ; code [ebp - 16]
read_char:
    mov     DWORD [ebp-12], 0     ; current bit = 0
	xor		edi, edi	    ; waiting until color change
	mov 	DWORD [ebp-16], 0		; code [ebp - 16]

read_bit_in_code:
	xor		edx, edx		; current space width
	shl		DWORD [ebp-16], 1		; make place for next bit in code
	inc 	edi     			; 0->1 1->0
	and		edi, 1
	
continue:	
	;checking pixel
	call 	is_pixel_black
	inc		edx
	inc 	ecx
	cmp		eax, edi
	jz		continue		; if color didnt change, continue counting
	
	cmp		edx, [ebp-8]
	jz		detected_wide_space		; if detected wide space, add 1 to code, else skip
	inc		DWORD [ebp - 16]

detected_wide_space:
	inc		DWORD [ebp-12]
	cmp		DWORD [ebp-12], 9
	jl		read_bit_in_code		; read next bit if number of bits is less than 9
	
	xor		edx, edx	; code of current character

check_next_row:		; iterate through code 39 table until matching code found
	cmp		edx, 44
	jz		processing_error ; exit if table out of range
	lea		eax, [table + edx * 4]
	mov		ebx, [eax]			; load value from table
	and		ebx, 1023	; only code sequence
	inc		edx
	cmp		ebx, [ebp - 16]
	jnz		check_next_row
	mov		ebx, [eax]
	shr		ebx, 16		; extract ascii code
	xor		eax, eax
	cmp		ebx, 42
	jz		end_func 		; if character is '*' end reading
	mov		edx, [ebp+24]
	add		edx, esi
	mov		[edx], ebx		; add character to result string 
	inc		esi
	add		ecx, [ebp-4]	; skip white space between characters
	jmp	    read_char
	
end_func:
	add     esp, 16     ; pop 4 local variables
	pop     ebx
	pop 	esi
	pop 	edi
    pop     ebp
    ret

processing_error:
	mov		eax, 1
	jp		end_func

; ============================================================================
is_pixel_black:
;	eax - 1 if pixel is black else 0

	mov     ebx, ebp        ; ebp to calculate place on stack
	push	ebp
	mov		ebp, esp
	
	mov		eax, DWORD [ebx+28]
	imul	eax, [ebx+36]
	add		eax, ecx
	lea 	eax, [eax + eax*2]
	add		eax, DWORD [ebx+20]
	; eax = (line_width * scanline + current_x ) * 3 + image_address
	; eax = address of pixel to check
	
	mov		al, [eax]
	inc     al
	and     eax, 1
	pop		ebp
	ret	

