#-------------------------------------------------------------------------------
#author: Marcin Giero�
#data : 2021.05.01
#description : reading code 39 from BMP file 600 x 50
#-------------------------------------------------------------------------------

#only 24-bits 600x50 pixels BMP files are supported
.eqv BMP_FILE_SIZE 90122
.eqv BYTES_PER_ROW 1800
.eqv TABLE_SIZE 176

	.data
.align 4
res:	.space 2
image:	.space BMP_FILE_SIZE
table:	.space TABLE_SIZE
result:	.space 400

fname:	.asciiz "kod.bmp"
errcd:	.asciiz	"Blad przetwarzania"
errfl:	.asciiz	"Zly format pliku"
errnf:	.asciiz "Nie udalo sie otworzyc pliku"
	.text
main:
	jal	read_bmp
	jal	load_code39
	
	li 	$s1, 0	# x coord of current processing pixel
	li	$s2, 0	# width of wide line
	li	$s3, 0	# width of narrow line
	
skipping_white_space:
	addi 	$s1, $s1, 1	#increment until black pixel is found
	#checking if pixel is black
	move	$a0, $s1
	li	$a1, 25
	jal     is_pixel_black
	#if pixel is not black, go back
	beqz 	$v0, skipping_white_space
	
	move	$s4, $s1	# number of first black pixel

counting_narrow:
	addiu 	$s1, $s1, 1	#increment until white pixel is found
	#checking if pixel is white
	move	$a0, $s1
	jal     is_pixel_black
	#if pixel is not white, go back
	bgtz 	$v0, counting_narrow
	
	sub	$s3, $s1, $s4	# calculating width of narrow space
	move	$s4, $s1
	
counting_wide:
	addiu 	$s1, $s1, 1	#increment until black pixel is found
	#checking if pixel is black
	move	$a0, $s1
	jal     is_pixel_black
	#if pixel is not white, go back
	beqz 	$v0, counting_wide
	sub	$s2, $s1, $s4	# calculating width of narrow space

	# skip rest of * character
	# skip two wide spaces
	sll 	$t1, $s2, 1
	add 	$s1, $s1, $t1
	# skip six narrow spaces (t1 = (s1 * 2 + s1) * 2)
	sll 	$t1, $s3, 1
	add	$t1, $t1, $s3 
	sll 	$t1, $t1, 1
	add 	$s1, $s1, $t1
	addiu	$s1, $s1, 1
	
	li	$t9, 0	# length of result 
read_char:
	li	$t5, 0 	# number of current bit
	li	$t6, 0	# waiting until color change
	li	$t8, 0	# code

read_bit_in_code:
	li	$t7, 0	# current space width
	sll	$t8, $t8, 1	# make place for next bit in code
	not 	$t6, $t6	# 0->1 1->0
	and	$t6, $t6, 1
	
continue:	
	#checking pixel
	move	$a0, $s1
	li	$a1, 25
	jal     is_pixel_black
	
	addiu	$t7, $t7, 1
	addiu	$s1, $s1, 1
	beq	$v0, $t6, continue	# if color didnt change, continue counting
		
	beq	$t7, $s2, detected_wide_space	# if detected wide space, add 1 to code, else skip
	addiu	$t8, $t8, 1

detected_wide_space:
	addiu	$t5, $t5, 1	
	blt	$t5, 9, read_bit_in_code	# read next bit if number of bits is less than 9

	li	$s5, 0	# code of current character

check_next_row:		# iterate through code 39 table until matching code found
	beq	$s5, 176, processing_error # exit if table out of range
	lw	$s6, table($s5) # load value from table
	and	$s7, $s6, 1023	# only code sequence
	addiu	$s5, $s5, 4	# set to new offset
	bne	$s7, $t8, check_next_row
	srl	$s6, $s6, 16	# extract ascii code
	beq	$s6, 42, print_result 	# if character is '*' end reading
	sb	$s6, result($t9) 	# add character to result string 
	addi	$t9, $t9, 1
	add	$s1, $s1, $s3	# skip white space between characters
	b	read_char

print_result:
	la	$a0, result
	jal 	exit

# ============================================================================
processing_error:
	la	$a0, errcd
	jal 	exit
	
# ============================================================================
file_format_error:
	la	$a0, errfl
	jal 	exit
	
# ============================================================================
file_open_error:
	la	$a0, errnf
	jal 	exit
	
# ============================================================================
exit:
#arguments:
#	$a0 - message to print

	li  	$v0, 4          # service 1 is print string
    	syscall
	li 	$v0, 10		#Terminate the program
	syscall

# ============================================================================
read_bmp:
#description: 
#	reads the contents of a bmp file into memory
#arguments:
#	none
#return value: none
	sub 	$sp, $sp, 4		#push $ra to the stack
	sw 	$ra,4($sp)
	sub 	$sp, $sp, 4		#push $s1
	sw 	$s1, 4($sp)
#open file
	li 	$v0, 13
        la 	$a0, fname		# file name 
        li 	$a1, 0		# flags: 0-read file
        li 	$a2, 0		# mode: ignored
        syscall
	move 	$s1, $v0      # save the file descriptor
	
	beq	$s1, -1, file_open_error 	# cannot open file

#read file
	li 	$v0, 14
	move 	$a0, $s1
	la 	$a1, image
	li 	$a2, BMP_FILE_SIZE
	syscall
	
	la	$t1, image + 18
	lw	$t2, ($t1)	# check if image width is 600
	bne	$t2, 600, file_format_error
	la	$t1, image + 22
	lw	$t2, ($t1)	# check if image height is 50
	bne	$t2, 50, file_format_error

#close file
	li 	$v0, 16
	move 	$a0, $s1
        syscall
	
	lw 	$s1, 4($sp)		#restore (pop) $s1
	add 	$sp, $sp, 4
	lw 	$ra, 4($sp)		#restore (pop) $ra
	add 	$sp, $sp, 4
	jr	$ra


# ============================================================================
is_pixel_black:
#description: 
#	returns if specified pixel is black
#arguments:
#	$a0 - x coordinate
#	$a1 - y coordinate - (0,0) - bottom left corner
#return value:
#	$v0 - 1 if pixel is black else 0

	sub 	$sp, $sp, 4		#push $ra to the stack
	sw 	$ra, 4($sp)

	la 	$t1, image + 10	#adress of file offset to pixel array
	lw 	$t2, ($t1)		#file offset to pixel array in $t2
	la 	$t1, image		#adress of bitmap
	add 	$t2, $t1, $t2	#adress of pixel array in $t2
	
	#pixel address calculation
	mul 	$t1, $a1, BYTES_PER_ROW #t1= y*BYTES_PER_ROW
	move 	$t3, $a0		
	sll	$a0, $a0, 1
	add 	$t3, $t3, $a0	#$t3= 3*x
	add 	$t1, $t1, $t3	#$t1 = 3x + y*BYTES_PER_ROW
	add 	$t2, $t2, $t1	#pixel address 
	
	li 	$v0, 1
	
	lbu 	$t4, ($t2)
	bgtz 	$t4, ret_0
	lbu 	$t4, 1($t2)
	bgtz 	$t4, ret_0
	lbu 	$t4, 2($t2)
	bgtz 	$t4, ret_0

	lw 	$ra, 4($sp)	#restore (pop) $ra
	add 	$sp, $sp, 4
	jr 	$ra

ret_0:
	li 	$v0, 0
	lw 	$ra, 4($sp)		#restore (pop) $ra
	add 	$sp, $sp, 4
	jr 	$ra

# ============================================================================

load_code39:
#description: 
#	loads the content of code39 table into memory
#arguments:
#	none
#return value: none
	sub 	$sp, $sp, 4		#push $ra to the stack
	sw 	$ra,4($sp)
	
	li $t1, 0x003001CB
	sw $t1, table
	li $t1, 0x003100DE
	sw $t1, table + 4
	li $t1, 0x0032019E
	sw $t1, table + 8
	li $t1, 0x0033009F
	sw $t1, table + 12
	li $t1, 0x003401CE
	sw $t1, table + 16
	li $t1, 0x003500CF
	sw $t1, table + 20
	li $t1, 0x0036018F
	sw $t1, table + 24
	li $t1, 0x003701DA
	sw $t1, table + 28
	li $t1, 0x003800DB
	sw $t1, table + 32
	li $t1, 0x0039019B
	sw $t1, table + 36
	li $t1, 0x004100F6
	sw $t1, table + 40
	li $t1, 0x004201B6
	sw $t1, table + 44
	li $t1, 0x004300B7
	sw $t1, table + 48
	li $t1, 0x004401E6
	sw $t1, table + 52
	li $t1, 0x004500E7
	sw $t1, table + 56
	li $t1, 0x004601A7
	sw $t1, table + 60
	li $t1, 0x004701F2
	sw $t1, table + 64
	li $t1, 0x004800F3
	sw $t1, table + 68
	li $t1, 0x004901B3
	sw $t1, table + 72
	li $t1, 0x004A01E3
	sw $t1, table + 76
	li $t1, 0x004B00FC
	sw $t1, table + 80
	li $t1, 0x004C01BC
	sw $t1, table + 84
	li $t1, 0x004D00BD
	sw $t1, table + 88
	li $t1, 0x004E01EC
	sw $t1, table + 92
	li $t1, 0x004F00ED
	sw $t1, table + 96
	li $t1, 0x005001AD
	sw $t1, table + 100
	li $t1, 0x005101F8
	sw $t1, table + 104
	li $t1, 0x005200F9
	sw $t1, table + 108
	li $t1, 0x005301B9
	sw $t1, table + 112
	li $t1, 0x005401E9
	sw $t1, table + 116
	li $t1, 0x0055007E
	sw $t1, table + 120
	li $t1, 0x0056013E
	sw $t1, table + 124
	li $t1, 0x0057003F
	sw $t1, table + 128
	li $t1, 0x0058016E
	sw $t1, table + 132
	li $t1, 0x0059006F
	sw $t1, table + 136
	li $t1, 0x005A012F
	sw $t1, table + 140
	li $t1, 0x002D017A
	sw $t1, table + 144
	li $t1, 0x002E007B
	sw $t1, table + 148
	li $t1, 0x0020013B
	sw $t1, table + 152
	li $t1, 0x00240157
	sw $t1, table + 156
	li $t1, 0x002F015D
	sw $t1, table + 160
	li $t1, 0x002B0175
	sw $t1, table + 164
	li $t1, 0x002501D5
	sw $t1, table + 168
	li $t1, 0x002A016B
	sw $t1, table + 172

	lw 	$ra, 4($sp)		#restore (pop) $ra
	add 	$sp, $sp, 4
	jr 	$ra

