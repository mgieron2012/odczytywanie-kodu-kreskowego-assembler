#include <stdio.h>
#include <stdlib.h>

extern "C" int Decode39(unsigned char *image, char *text, int xsize, int ysize, int scanline);

#define IMAGE_WIDTH 600
#define IMAGE_HEIGHT 50

int table[44]{
        0x003001CB,
        0x003100DE,
        0x0032019E,
        0x0033009F,
        0x003401CE,
        0x003500CF,
        0x0036018F,
        0x003701DA,
        0x003800DB,
        0x0039019B,
        0x004100F6,
        0x004201B6,
        0x004300B7,
        0x004401E6,
        0x004500E7,
        0x004601A7,
        0x004701F2,
        0x004800F3,
        0x004901B3,
        0x004A01E3,
        0x004B00FC,
        0x004C01BC,
        0x004D00BD,
        0x004E01EC,
        0x004F00ED,
        0x005001AD,
        0x005101F8,
        0x005200F9,
        0x005301B9,
        0x005401E9,
        0x0055007E,
        0x0056013E,
        0x0057003F,
        0x0058016E,
        0x0059006F,
        0x005A012F,
        0x002D017A,
        0x002E007B,
        0x0020013B,
        0x00240157,
        0x002F015D,
        0x002B0175,
        0x002501D5,
        0x002A016B
};

  int main(int argc, char** argv) {
  if(argc != 3){
    printf("Incorrect args\n");
    return 1;
  }

  char* filename = argv[1];
  int scanline = atoi(argv[2]);
  int offsetByte = 11;
  unsigned char buffer[0x20];
  FILE* imageFile = fopen(filename, "rb");
  if (imageFile == NULL){
    printf("No such file or directory\n");
    return 1;
  }

  fread(buffer, 1, 0x20, imageFile);

  if(buffer[0] != 0x42 || buffer[1] != 0x4d){
        printf("Incorrect file format\n");
        return 1;
  }

  int real_width = buffer[0x12] + buffer[0x13] * 256;
  int real_height = buffer[0x16] + buffer[0x17] * 256;
  int offset = buffer[0xa] + buffer[0xb] * 256;

  if(real_width != IMAGE_WIDTH || real_height != IMAGE_HEIGHT){
    printf("Only support 600 x 50 images\n");
    return 1;
  }

  int imageSize = IMAGE_WIDTH * IMAGE_HEIGHT * 3;
  unsigned char image[imageSize];
  fread(image, 1, offset - 0x20, imageFile);
  fread(image, 1, imageSize, imageFile);
  fclose(imageFile);
  char result[100] = "";

  int error = Decode39(image, result, IMAGE_WIDTH, IMAGE_HEIGHT, scanline);

  if(error){
    printf("Processing error\n");
    return 1;
  }
  printf("%s \n", result);
  return 0;
}


